const form = document.querySelector("#form");

const validateForm = (event) => {
    let phoneNum = document.querySelector('#phone').value;
    const regexPattern = /^([(][0-9]{3}[)]|([0-9]{3}))[-\s]?[0-9]{3}[-\s]?[0-9]{4}$/;
    if(!regexPattern.test(phoneNum)){
        document.querySelector(".errorlabel").style.display="inline-block";
        let phoneError = document.querySelectorAll('.phone'); 
        for (var i = 0; i < phoneError.length; ++i) {
            phoneError[i].classList.add('error');
        }
        return false;
    }
    else
    {
        //Submit form here
        //Simulating form submitted
        document.querySelector(".title h2").innerHTML = "Thank you!";
        document.querySelector(".title_d h2").style.display="inline-block";
        document.querySelector("#form").style.display="none";
        return false;
    }
}

form.onsubmit = () => validateForm();

